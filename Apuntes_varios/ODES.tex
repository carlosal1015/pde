\documentclass[10pt]{amsart}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
%\newtheorem{lemma}{theorem}[Lemma]
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
%\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\title{ODE}
\author{Oscar Reula}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle
\section{Preliminary material}

\subsection{Linear equations with sources}

We shall look now at the solutions to:

\[
y_t = \lambda y + a e^{\mu t} \;\;\;\;\; y(0) = y_0, \;\;\; \lambda \neq \mu
\]

The last term is called a forcing term. The corresponding inhomogeneous solution the forced solution. 

\[
y_p(t) := Ae^{\mu t},
\]
%
plugging it into the equation we get, 

\[
A\mu e^{\mu t} = \lambda A e^{\mu t} + a e^{\mu t} \;\;\;\; A(\mu - \lambda) = a, \;\;\;\;\; A = \frac{a}{\mu - \lambda}
\]
%
Thus, 
\[
y_p(t) := \frac{a}{\mu - \lambda}e^{\mu t},
\]
%

When the forcing frequency is resonant ($\lambda = \mu$) then the solution can be obtained as a limit. 
In that case it is given by,

\[
y_p(t) := A t e^{\lambda t}, \;\;\; A= a.
\]
%
It grows faster than the homogeneous solution. 

\[
y(t) = y_h(t) + y_p(t),
\]

\[
y_h(0) = y_o - y_p(0) = \left \{ \begin{array}{cc}
                                                y_o - \frac{a}{\mu - \lambda} & \mu \neq \lambda \\
                                                y_o & \mu = \lambda
                                                \end{array}
                                                \right.
\]                                                

\begin{eqnarray}
y(t) &=& (y_o - \frac{a}{\mu-\lambda})e^{\lambda t} + \frac{a}{\mu-\lambda} e^{\mu t} \\
      &=& [(y_o - \frac{a}{\mu-\lambda}) + \frac{a}{\mu - \lambda}]e^{\lambda t} + \frac{a}{\mu - \lambda}[e^{\mu t}-e^{\lambda t}] \\
      &=& y_o e^{\lambda t} + \frac{a}{\mu - \lambda}e^{\lambda t}[e^{(\mu - \lambda) t} - 1] \\
      &=& y_o e^{\lambda t} + ta e^{\lambda t} \;\;\; \mbox{in the limit} \;\;\; \mu \to \lambda
\end{eqnarray}

Resonances  would appear latter when studying hyperbolic systems.

In general, for a system of the form:


\[
y_t = \lambda y + F(t)
\]

The solution is given by,

\[
y(t) = e^{\lambda t} y_o + \int_0^t e^{\lambda(t - \zeta)} F(\zeta) \; d\zeta
\]

This expression can be further generalized noticing that if we define the map that takes an homogeneous solution at time $\zeta$ and move it to time $t$, 

\[
y(t) = S(t,\zeta) y(\zeta).
\]
%
then 
\[
y_h(t) = e^{\lambda t} y_o = e^{\lambda(t-\zeta)}e^{\zeta t} y_o = e^{\lambda(t-\zeta)} y_h(\zeta) = S(t,\zeta)y_h(\zeta)
\]
%
and
\[
y(t) = S(t,0)y_o + \int_0^t S(t,\zeta)F(\zeta) \; d\zeta
\]

If we consider now the equation,

\[
y_t = a(t) y + F(t)
\]
%
and we define the homogeneous map as before, then, $y_h(t) = S(t,\zeta)y_h(zeta)$, then
the solution of the inhomogeneous equation is as before:

\[
y(t) = S(t,0)y_o + \int_0^t S(t,\zeta)F(\zeta) \; d\zeta.
\]
%
To see this notice that
\[
S(t,\zeta)_t = a(t)S(t,\zeta),\;\;\;  \mbox{and} \;\; S(\zeta,\zeta)=id
\]

and so,

\begin{eqnarray}
y_t(t) &=&  a(t)S(t,0) + S(t,t)F(t) + \int_0^t a(t) S(t,\zeta) F(\zeta) \; d\zeta \\
         &=&  a(t)[S(t,0)  + \int_0^t S(t,\zeta) F(\zeta) \; d\zeta] + S(t,t)F(t) \\
         &=&  a(t)y(t) + F(t)
\end{eqnarray}

But the homogeneous solution is also easy to find,

\[
S(t,\zeta) = e^{\int_{\zeta}^t a(\tau) \; d\tau}
\]
% 
indeed, 

\[
S(t,\zeta)_t = a(t)e^{\int_{\zeta}^t a(\tau) \; d\tau} = a(t)S(t,\zeta)
\]


\section{One step methods: Euler's Explicit Method}

We first discretize time, we let $t_n = t_o + dt n$ if the discretization is constant or $t_n = t_o \sum_{i=0}^{n} dt_i$ if the successive time steps are denoted by $dt_i$.
Thus we have a discrete grid, $G \subset Z \to R$. 
Thus, if we have a function $y: I \to R$ we shall denote by $y_n$ the restriction of that function to the grid $G$, $y_n = y(t_n)$. 

Given an ordinary differential equation, (here we can consider $y$ to be a vector in some region of $R^n$)

\[
y_t = F(y)\;\;\;\;\; y(0)=y_o
\]
%
Euler's explicit method approximate the time derivative by,

\[
y_t(t_n) = \frac{y(t_{n+1}) - y(t_n)}{dt_n} = \frac{y_{n+1} - y_n}{dt_n},
\]

therefore we have,

\[
y_{n+1} = y_{n} + dt_n F(y_n),
\]
% 
which can be solved recursively starting with $y_0 = y_o$. If $F=F(y,t)$ then we extend the system with an extra variable $t$ and use the same formulae.






\subsection{Example}


\[
y_t =  \lambda y + a e^{\mu t}
\]

We start with the homogeneous equation. Euler's method gives, (for $dt$ constant)

\[
y^h_{n+1} = y^h_{n} + dt \lambda y^h_n = (1+ dt \lambda)y^h_n
\]
%
which can be solved to be,

\[
y^h_n = (1+dt \lambda)^n (y_o - y^p_o)
\]
%
where $y^p_o$ is the initial value of the particular solution to be found next.

Notice that, using $\ln(1+\varepsilon) = \ln(1) + \frac{\varepsilon}{1} -  \frac{\varepsilon^2}{2} + \cdots$, $1+\varepsilon = e^{\varepsilon} e^{O(\varepsilon^2)}$,

\[
(1+dt\lambda)^n = (1+dt\lambda)^{t_n/dt} = (e^{dt \lambda}e^{O(dt^2 \lambda^2)})^{(t_n/dt)} = e^{\lambda t_n} e^{O(dt \lambda^2 t_n)}
\]
%
Therefore, 

\[
y^h_n = e^{\lambda t_n} e^{dt \lambda^2 t_n}(y_o - y^p_o) = e^{\lambda t_n} [1+ O({dt \lambda^2 t_n})](y_o - y^p_o) = y^h(t_n) + e^{\lambda t_n} O({dt \lambda^2 t_n})(y_o - y^p_o),
\]
% 
or
\[
max_{0\leq t_n \leq T}|y^h_n - y^h(t_n)| \leq C(\lambda T) dt
\]

We now look at the particular solution: in Euler's approximation we get,

\[
y^p_{n+1} = y^p_n + dt[\lambda y^p_n + ae^{\mu t_n}].
\]
%
As in the analytic case we look for a solution of the form,

\[
y^p_n = \tilde{A}e^{\mu t_n}.
\]
%
Substitution in the recursion gives,

\[
\tilde{A}e^{\mu(t_n + dt)} = (1 + dt \lambda)Ae^{\mu t_n} + dt a e^{\mu t_n}, \;\;\;\;\; \tilde{A} = \frac{dt a}{e^{\mu dt} - (1+ dt \lambda)}
\]

Using that $e^{\mu dt} = 1 + \mu dt + \frac{\mu^2 dt^2}{2} + O(\mu^3 dt^3)$ we have, 

\[
\tilde{A} = \frac{a}{\mu - \lambda + \frac{\mu^2 dt}{2} + O(\mu^3 dt^2)}
\]
and so, 

\begin{eqnarray}
y^p_n &=& \frac{ae^{\mu t_n}}{\mu - \lambda + \frac{\mu^2 dt}{2} + O(\mu^3 dt^2)} \\
          &=& y^p(t_n) + ae^{\mu t_n} [\frac{1}{\mu - \lambda + \frac{\mu^2 dt}{2} + O(\mu^3 dt^2)}  - \frac{1}{\mu - \lambda}]\\
          &=& y^p(t_n) - \frac{dt}{2}\frac{\mu^2}{(\mu - \lambda)^2}ae^{\mu t_n} + O(\mu dt^2) \frac{\mu^2}{(\mu - \lambda)^2}ae^{\mu t_n}
\end{eqnarray}          


Therefore, in a finite interval $[0,T]$ we have,
\[
max_{0\leq t_n \leq T} |y^p(t_n) - y^p_n| \leq C(\lambda, \mu, T) dt
\]

Adding the two solutions with the correct boundary conditions we find that for finite time intervals the error is bounded linearly in $dt$ and so, taking
$dt$ small enough we can approximate the solution as much as we want. 

So we have the following lemma:

\begin{lemma} Let $y_t = \lambda y + ae^{\mu t}$, $\mu \neq \lambda$, T>0, and $y_o$. Then for some constant $C$ Euler's approximation satisfies
\[
max_{0\leq t_n \leq T} |y(t_n) - y_n| \leq C dt.
\]
For the case $\Re(\lambda) < 0$, $\Re(\mu) < 0$ we can bound the solution for all times.

\end{lemma} 

If we use a further order for the homogeneous approximation, namely, 

\begin{eqnarray}
(1+dt\lambda)^n &=& (1+dt\lambda)^{t_n/dt} \\
 			  &=& (e^{dt \lambda}e^{-\frac{dt^2 \lambda^2}{2}}e^{O(dt^3\lambda^3)})^{(t_n/dt)} \\
			  &=& e^{\lambda t_n} e^{-\frac{dt \lambda^2 t_n}{2}} e^{O(dt^2\lambda^3 t_n}) \\
			  &=& e^{\lambda t_n} [1 - \frac{dt \lambda^2 t_n}{2} + O(dt^2 \lambda^3)]
\end{eqnarray}
%
we can refine the error expression as follows:

\[
y(t_n)-y_n = - \frac{dt}{2}[\lambda^2 t_n e^{\lambda t_n} + \frac{a \mu^2}{(\mu - \lambda)^2}e^{\mu t_n}] + O(dt^2) = dt \phi_1(t_n) + O(dt^2).
\]
%
where the function $\phi_1$ does not depends on $dt$.





\end{document}