FROM buildpack-deps:bionic

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -qq update && \
    apt-get -qq install --yes --no-install-recommends locales > /dev/null && \
    apt-get -qq purge && \
    apt-get -qq clean && \
    rm -rf /var/lib/apt/lists/*

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV SHELL /bin/bash

ARG NB_USER=jovyan
ARG NB_UID=1000

ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}

RUN groupadd \
    --gid ${NB_UID} \
    ${NB_USER} && \
    useradd \
    --comment "Default user" \
    --create-home \
    --gid ${NB_UID} \
    --no-log-init \
    --shell /bin/bash \
    --uid ${NB_UID} \
    ${NB_USER}

RUN wget --quiet -O - https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - && \
    DISTRO="bionic" && \
    echo "deb https://deb.nodesource.com/node_10.x $DISTRO main" >> /etc/apt/sources.list.d/nodesource.list && \
    echo "deb-src https://deb.nodesource.com/node_10.x $DISTRO main" >> /etc/apt/sources.list.d/nodesource.list

RUN apt-get -qq update && \
    apt-get -qq install --yes --no-install-recommends \
    less \
    nodejs \
    unzip \
    > /dev/null && \
    apt-get -qq purge && \
    apt-get -qq clean && \
    rm -rf /var/lib/apt/lists/*

EXPOSE 8888

ENV APP_BASE /srv
ENV NPM_DIR ${APP_BASE}/npm
ENV NPM_CONFIG_GLOBALCONFIG ${NPM_DIR}/npmrc
ENV CONDA_DIR ${APP_BASE}/conda
ENV NB_PYTHON_PREFIX ${CONDA_DIR}/envs/notebook
ENV KERNEL_PYTHON_PREFIX ${NB_PYTHON_PREFIX}
ENV JULIA_PATH ${APP_BASE}/julia
ENV JULIA_DEPOT_PATH ${JULIA_PATH}/pkg
ENV JULIA_VERSION 1.5.0
ENV JUPYTER ${NB_PYTHON_PREFIX}/bin/jupyter
ENV JUPYTER_DATA_DIR ${NB_PYTHON_PREFIX}/share/jupyter
ENV PATH ${NB_PYTHON_PREFIX}/bin:${CONDA_DIR}/bin:${NPM_DIR}/bin:${JULIA_PATH}/bin:${PATH}

COPY docker_scripts/activate-conda.sh /etc/profile.d/activate-conda.sh
COPY docker_scripts/environment.yml /tmp/environment.yml
COPY docker_scripts/install-miniforge.bash /tmp/install-miniforge.bash

RUN mkdir -p ${NPM_DIR} && chown -R ${NB_USER}:${NB_USER} ${NPM_DIR}

USER ${NB_USER}

RUN npm config --global set prefix ${NPM_DIR}

USER root

RUN bash /tmp/install-miniforge.bash && rm /tmp/install-miniforge.bash /tmp/environment.yml

RUN mkdir -p ${JULIA_PATH} && \
    curl -sSL "https://julialang-s3.julialang.org/bin/linux/x64/${JULIA_VERSION%[.-]*}/julia-${JULIA_VERSION}-linux-x86_64.tar.gz" | tar -xz -C ${JULIA_PATH} --strip-components 1

RUN mkdir -p ${JULIA_DEPOT_PATH} && \
    chown ${NB_USER}:${NB_USER} ${JULIA_DEPOT_PATH}

ARG REPO_DIR=${HOME}

ENV REPO_DIR ${REPO_DIR}

WORKDIR ${REPO_DIR}

ENV PATH ${HOME}/.local/bin:${REPO_DIR}/.local/bin:${PATH}
ENV CONDA_DEFAULT_ENV ${KERNEL_PYTHON_PREFIX}
ENV JULIA_PROJECT ${REPO_DIR}

COPY requirements.txt ${REPO_DIR}/requirements.txt

USER root

RUN chown -R ${NB_USER}:${NB_USER} ${REPO_DIR}

USER ${NB_USER}

RUN ${KERNEL_PYTHON_PREFIX}/bin/pip install --no-cache-dir -r ${REPO_DIR}/requirements.txt

USER root

COPY . ${REPO_DIR}

RUN chown -R ${NB_USER}:${NB_USER} ${REPO_DIR}

USER ${NB_USER}

RUN JULIA_PROJECT="" julia -e "using Pkg; Pkg.add(\"IJulia\"); using IJulia; installkernel(\"Julia\", \"--project=${REPO_DIR}\");" && julia --project=${REPO_DIR} -e 'using Pkg; Pkg.instantiate(); Pkg.resolve(); pkg"precompile"'

USER ${NB_USER}

RUN chmod +x ${REPO_DIR}/postBuild
RUN ${REPO_DIR}/postBuild

COPY docker_scripts/entrypoint /usr/local/bin/entrypoint

ENTRYPOINT ["/usr/local/bin/entrypoint"]

CMD ["jupyter", "notebook", "--ip", "0.0.0.0"]
