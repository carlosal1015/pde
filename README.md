# Ecuaciones diferenciales en derivadas parciales 2020

## Instalación

Para ejecutar estos notebooks de manera directa se recomienda emplear [Binder]. Haga click en el logo a continuación.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/oreula%2Fpde.git/master)

Para una instalación local, léa las instrucciones en el archivo [INSTALL.md].


[Binder]: https://mybinder.org
[INSTALL.md]: ./INSTALL.md
