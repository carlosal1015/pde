# Instalación

A continuación se detallan distintos procedimientos de instalación para poder ejecutar ejecutar
los notebooks del curso.

## Instalación con Docker

### Requerimientos

Para poder ejecutar esta instalacion requiere tener instalado [Docker]. Visite la página oficial
[aquí][Docker] para elegir la instalacion adecuada para su sistema.

La imagen del curso tiene un peso de alrededor de 6GB por lo que se recomienda que tenga almenos
18GB disponibles.

### Instalación

1. Construir la imagen

```
docker build -f Dockerfile -t pde2020 .
```

2. Ejecutar la imagen

```
docker run -p 8888:8888 pde2020
```

3. Copiar la URL que muestra en la terminal y pegarla en el navegador.


### Notas

- ¿Quiero usar `jupyterlab`?

  Para usa usar `jupyterlab`, solo cambie la terminación **tree** de la URL por la terminación
  **lab**.

  ```
  # Jupyter Notebook
  http://127.0.0.1:8888/tree
  ```

  ```
  # JupyterLab
  http://127.0.0.1:8888/lab
  ```
