{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Wave equation using finite differences and method of lines. \n",
    "\n",
    "### Oscar Reula"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We shall use the Julia Packet DifferentialEquations.jl \n",
    "I am modifying examples given in these two pages:\n",
    "\n",
    "https://tutorials.sciml.ai/html/introduction/03-optimizing_diffeq_code.html\n",
    "\n",
    "http://juliamatrices.github.io/BandedMatrices.jl/latest/#Creating-banded-matrices-1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We shall be solving hyperbolic equations by **the method of lines** and **finite-differences** approximations. This means that a system like this:\n",
    "\n",
    "\\begin{equation}\n",
    "u_t = u_x\n",
    "\\end{equation}\n",
    "\n",
    "will be first (conceptually) approximated by a finite-difference operator, $D_x$, on the space direction, so as to consider the following system:\n",
    "\n",
    "\\begin{equation}\n",
    "v_t = D_x\\; v.\n",
    "\\end{equation}\n",
    "\n",
    "Where $v$ is a discrete version of $u$ *in space only*. That is, $v$ is an $N$ dimensional vector (if we are taking a grid of $N$ points). \n",
    "This way we get a system of ordinary differential equations of dimension $N$. We then proceed to approximate this system using an appropriate ODE integrator. This way we end up with a discretization in space and time.\n",
    "\n",
    "We are going to solve the 2-dimensional wave equation, namely,\n",
    "\n",
    "\\begin{equation}\n",
    "\\phi_{tt} = \\phi_{xx}\n",
    "\\end{equation}\n",
    "\n",
    "To put it in the estandard form we define two new variables,\n",
    "$u := \\phi_x$, and $v := \\phi_t$. The above system then becomes, \n",
    "\n",
    "\\begin{eqnarray}\n",
    "\\phi_t & = & v \\\\\n",
    "v_t & = & u_x \\\\\n",
    "u_t & = & v_x,\n",
    "\\end{eqnarray}\n",
    "where we have used that $u_t := \\phi_{xt} = \\phi_{tx} := v_x$, and $v_t :=\\phi_{tt} = \\phi_{xx} = u_x$.  \n",
    "\n",
    "Since the equation for $\\phi$ can be integrated once we know $(u,v)$, and it is not need to solve the rest of the system, we ignore it from now on. \n",
    "\n",
    "If we define $u^+:= u+v$ and $u^- = u-v$ we can see that we diagonalize the above system, that is, in term of these variables the system becomes,\n",
    "\n",
    "\\begin{eqnarray}\n",
    "u^+_t & = & u^+_x \\\\\n",
    "u^-_t & = & -u^-_x,\n",
    "\\end{eqnarray}\n",
    "\n",
    "Thus, we have two independent waves, one moving to the right and the other to the left, that is $u^+(t,x) = f^+(t+x)$ (moving to the left (smaller $x$'s)) and \n",
    "$u^-(t,x) = f^-(t-x)$ (moving to the right). \n",
    "\n",
    "So our numerical solutions would be linear combinations of these two solutions, depending on the initial data. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are using Julia, so we will upload some packages, the set of ordinary differential equations integrators, some help for plotting results, and some libraries for handling matrices, in particular sparce matrices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#using DiffEqOperators\n",
    "using OrdinaryDiffEq\n",
    "#using DifferentialEquations\n",
    "using Plots\n",
    "using LinearAlgebra\n",
    "using BandedMatrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we add some parameters for the simulation. Some are set to arbitrary values, just to indicate that you can add them if needed. $N$ is the number of point of our space discretization. We are going to be solving a *periodic* problem, so we start with point 1 and finish with point $N$, the point $N+1$ is identified with the point $1$ and so on. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 200 # number of space-points for the space discretization\n",
    "L = 1. # the space interval #\n",
    "dx = L/N\n",
    "T = 10. # final time integratios\n",
    "dt = 1. *dx # we take a dt around the size of dx/speed_max, \n",
    "            # so that the algorith is stable, the CFL condition.\n",
    "p = (1.0,1.0,dx) # some parameters a,dx\n",
    "\n",
    "r0=zeros(N,2) #the field discretizations, $u(t,(i-1)*dx)$ and $v(t,(i-1))$\n",
    "x = zeros(N) # the x coordinate at those points, needed to prescribe initial data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now define the finite-difference schemes. They are implemented as matrices multiplyting the vector solution components. The first two cases are for the second order scheme. They differ in the use of sparce matrices. This is important for efficient, and large systems, here, for the small systems (1D) we are implementing, it does not seems to be of importance. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# second order accurate scheme (sparce version)\n",
    "D_2_per = Array(Tridiagonal([-0.5 for i in 1:N-1],[0.0 for i in 1:N],[0.5 for i in 1:N-1]))\n",
    "D_2_per[1,end] = -0.5\n",
    "D_2_per[end,1] = 0.5\n",
    "    \n",
    "# second order accurate scheme (band version)\n",
    "D_2_per_b = BandedMatrix{Float64}(Zeros(N,N), (N-1,N-1))\n",
    "D_2_per_b[band(0)] .= 0\n",
    "D_2_per_b[band(1)] .= 0.5\n",
    "D_2_per_b[band(-1)] .= -0.5\n",
    "D_2_per_b[band(N-1)] .= -0.5\n",
    "D_2_per_b[band(-N+1)] .= 0.5\n",
    "\n",
    "# fourth order accurate scheme (band version)\n",
    "D_4_per = BandedMatrix{Float64}(Zeros(N,N), (N-1,N-1))\n",
    "D_4_per[band(0)] .= 0\n",
    "D_4_per[band(1)] .= 2/3\n",
    "D_4_per[band(-1)] .= -2/3\n",
    "D_4_per[band(2)] .= -1/12\n",
    "D_4_per[band(-2)] .= 1/12\n",
    "D_4_per[band(N-1)] .= -2/3\n",
    "D_4_per[band(-N+1)] .= 2/3\n",
    "D_4_per[band(N-2)] .= 1/12\n",
    "D_4_per[band(-N+2)] .= -1/12\n",
    "\n",
    "# second order accurate second-derivative finite difference approximation \n",
    "# to be used as an example in parabolic systems or to implement simple Kreiss-Oliger \n",
    "# dissipation\n",
    "\n",
    "Δ_2_per = Array(Tridiagonal([1.0 for i in 1:N-1],[-2.0 for i in 1:N],[1.0 for i in 1:N-1]))\n",
    "Δ_2_per[1,end] = 1.0\n",
    "Δ_2_per[end,1] = 1.0\n",
    "\n",
    "#print(D_2_per_b)\n",
    "#Δ_2_per;\n",
    "#print(D_4_per)\n",
    "#D_4_per"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define now the rhs of the equations in the method of lines, that is, the space discretization. These versions are for efficiency, and for further modifications. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function F2!(dr,r,p,t)\n",
    "    # second order version\n",
    "    a,α,dx = p\n",
    "    h = 1. /dx\n",
    "    u = @view r[:,1]\n",
    "    v = @view r[:,2]\n",
    "    du = @view dr[:,1]\n",
    "    dv = @view dr[:,2]\n",
    "    Du = h * D_2_per * v\n",
    "    Dv = h * D_2_per * u\n",
    "    @. du = Du\n",
    "    @. dv = Dv\n",
    "    #dr[:,1] .= dx * D_2_per * r[:,2]\n",
    "    #dr[:,2] .= - dx * D_2_per * r[:,1]\n",
    "end\n",
    "function F4!(dr,r,p,t)\n",
    "    # forth order version\n",
    "    a,α,dx = p\n",
    "    h = 1. /dx\n",
    "    u = @view r[:,1]\n",
    "    v = @view r[:,2]\n",
    "    du = @view dr[:,1]\n",
    "    dv = @view dr[:,2]\n",
    "    Du = h * D_4_per * v\n",
    "    Dv = h * D_4_per * u\n",
    "    @. du = Du\n",
    "    @. dv = Dv\n",
    "    #dr[:,1] .= dx * D_2_per * r[:,2]\n",
    "    #dr[:,2] .= - dx * D_2_per * r[:,1]\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now give initial data. The one below is such that the initial value for $u^+$ vanishes. Thus, this combination would remain zero for all times, and so, the only wave is the one going to the right, namely $u^-$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#n = Array(Vector([1.0 for i in 1:N]))\n",
    "#x = range(0., length=N, step=dx)\n",
    "#ones = ones(N)\n",
    "\n",
    "x0 = 0.4; x1 = 0.6\n",
    "for i in 1:N\n",
    "    x[i] = dx*(i-1)\n",
    "    if x[i] > x0 && x[i] < x1\n",
    "        r0[i,1] = (x[i] - x0)^4 * (x[i] - x1)^4 / (x1-x0)^8 * 250\n",
    "        r0[i,2] = -r0[i,1]\n",
    "    end\n",
    "end\n",
    "\n",
    "plot(r0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define now the two problems, for different accuracy. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@time prob2 = ODEProblem(F2!,r0,(0.0,T),p);\n",
    "@time prob4 = ODEProblem(F4!,r0,(0.0,T),p);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now solve them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@time sol2 = solve(prob2,RK4(),dt=dt);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@time sol4 = solve(prob4,RK4(),dt=dt);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally we plot the solutions at different times"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot([r0[:,1],sol4(T*0.01)[:,1],sol4(T*0.02)[:,1],sol4(T*0.03)[:,1],sol4(T*0.04)[:,1]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "anim = @animate for i ∈ 1:20\n",
    "    plot(sol4(T*0.005*i)[:,1])\n",
    "end\n",
    "\n",
    "gif(anim, \"wave_anim_fps20.gif\", fps = 20)\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot([r0[:,1],sol2(T)[:,1],sol4(T)[:,1]])\n",
    "#plot(x,sol.u)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.1",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
