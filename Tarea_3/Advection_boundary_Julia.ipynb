{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Advection equation with boundary using finite differences and method of lines. \n",
    "\n",
    "### Oscar Reula"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We shall be solving the advection equation by **the method of lines** and **finite-differences** approximations. Furthermore, since there will be boundaries, we shall use **penalty methods** to impose the values of the fields at boundaries.\n",
    "\n",
    "We shall solve the advection equation:\n",
    "\n",
    "\\begin{equation}\n",
    "u_t = -c u_x\n",
    "\\end{equation}\n",
    "\n",
    "We put a minus sign (for $c>0$) so that the wave propagates to the right. We are going to be solving it in a line segment, $[0,L]$. Since the wave travels to the right we shall need to give boundary values at the left $(x=0)$ boundary.\n",
    "We shall first (conceptually) approximated by a finite-difference operator, $D_x$, on the space direction, so as to consider the following system:\n",
    "\n",
    "\\begin{equation}\n",
    "v_t = c D_x\\; v.\n",
    "\\end{equation}\n",
    "\n",
    "Where $v$ is a discrete version of $u$ *in space only*. That is, $v$ is an $N$ dimensional vector (if we are taking a grid of $N$ points). \n",
    "This way we get a system of ordinary differential equations of dimension $N$. We then proceed to approximate this system using an appropriate ODE integrator. This way we end up with a discretization in space and time.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are using Julia, so we will upload some packages, the set of ordinary differential equations integrators, some help for plotting results, and some libraries for handling matrices, in particular sparce matrices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#using DiffEqOperators\n",
    "using OrdinaryDiffEq\n",
    "#using DifferentialEquations\n",
    "using Plots\n",
    "using LinearAlgebra\n",
    "using BandedMatrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we add some parameters for the simulation. Some are set to arbitrary values, just to indicate that you can add them if needed. $N$ is the number of point of our space discretization. We are solving in an interval, so we start with point 1 and finish with point $N$, at $L$. Thus, now $dx = L/(N-1)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 400 # number of space-points for the space discretization\n",
    "L = 1. # the space interval #\n",
    "dx = L/(N-1)\n",
    "T = 10. # final time integratios\n",
    "dt = 0.01 *dx # we take a dt around the size of dx/speed_max, \n",
    "            # so that the algorith is stable, the CFL condition.\n",
    "p = (1.0,5.0*2π,dx) # some parameters c, ω, dx\n",
    "\n",
    "u0=zeros(N) #the field discretizations, $u(t,(i-1)*dx)$ and $v(t,(i-1))$\n",
    "x = zeros(N); # the x coordinate at those points, needed to prescribe initial data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now define the finite-difference schemes. They are implemented as matrices multiplyting the vector solution components. We shall do the second order scheme which is simple. Instead of the operator we used for the periodic case here we shall use finite-differences operators satisfying **S**um **B**y **P**arts.\n",
    "\n",
    "These are operators with satisfy the same properties as the derives when integrated, namely \n",
    "\n",
    "$$\n",
    "\\int_a^b [u(x) \\frac{dv(x)}{dx} + \\frac{du(x)}{dx} v(x)]\\; dx = v(b)u(b) - v(a)u(a)\n",
    "$$\n",
    "\n",
    "At the discrete level this relation becomes:\n",
    "\n",
    "$$\n",
    "\\sum_{ij} h^{ij}[u_i (Dv)_j + (Du)_i v_j] = u[N]v[N] - u[1]v[1]\n",
    "$$\n",
    "\n",
    "They come into pairs, $(h,D)$. For second and fourth order approximations the pairs are unique, but for higher orders there is freedom. $h$ can be a diagonal bilinear form or can be more complicated. For higher dimensional simulations this is important. We shall stick here with the diagonal ones, although are not the most accurate. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# second order accurate scheme (sparce version)\n",
    "D_2_sbp = Array(Tridiagonal([-0.5 for i in 1:N-1],[0.0 for i in 1:N],[0.5 for i in 1:N-1]))\n",
    "D_2_sbp[1,1] = -1.\n",
    "D_2_sbp[1,2] = 1.\n",
    "D_2_sbp[end,end] = 1.\n",
    "D_2_sbp[end,end-1] = -1.\n",
    "#D_2_sbp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "h2 = Array(Diagonal([1. for i in 1:N]))\n",
    "h2[1,1] = 0.5\n",
    "h2[N,N] = 0.5\n",
    "#h2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can check that the relation holds:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u = rand(N)\n",
    "v = rand(N)\n",
    "\n",
    "(h2 * (D_2_sbp * v))' * u +  (h2 * (D_2_sbp * u))' * v - (u[N]*v[N] - u[1]*v[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The boundary condition at $x=0$ must be incorporated in the right hand side of the equation as a penalty term. In this case we add a term for the first component:\n",
    "\n",
    "$$\n",
    "\\frac{c}{h2[1,1]dx}(g(t) - u[1])\n",
    "$$\n",
    "\n",
    "where $g(t)$ is the boundary value we want to impose. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define now the rhs of the equations in the method of lines, that is, the space discretization. \n",
    "Notice that now we add a term for the first component, this is the SAP (or penalty) term. We choose $g(t)=sin(\\omega t)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function F2_sbp!(du,u,p,t)\n",
    "    # second order version\n",
    "    c,ω,dx = p\n",
    "    h = 1. /dx\n",
    "    Du = -c * h * D_2_sbp * u\n",
    "    #Du[1] = Du[1] + c*2. *h *(0.0 - u[1]) \n",
    "    Du[1] = Du[1] + c*2. *h *(sin(ω *t) - u[1])\n",
    "    @. du = Du\n",
    "end\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now give initial data. The one below is such that the initial value for $u^+$ vanishes. Thus, this combination would remain zero for all times, and so, the only wave is the one going to the right, namely $u^-$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#n = Array(Vector([1.0 for i in 1:N]))\n",
    "#x = range(0., length=N, step=dx)\n",
    "#ones = ones(N)\n",
    "\n",
    "x0 = 0.4; x1 = 0.6\n",
    "for i in 1:N\n",
    "    x[i] = dx*(i-1)\n",
    "    if x[i] > x0 && x[i] < x1\n",
    "        u0[i] = 0.0 #(x[i] - x0)^4 * (x[i] - x1)^4 / (x1-x0)^8 * 250\n",
    "    end\n",
    "end\n",
    "\n",
    "plot(u0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define now the two problems, for different accuracy. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@time prob2 = ODEProblem(F2_sbp!,u0,(0.0,T),p);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now solve them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@time sol2 = solve(prob2,RK4(),dt=dt);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally we plot the solutions at different times"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot([u0[:],sol2(T*0.1)[:],sol2(T*0.2)[:],sol2(T*0.3)[:],sol2(T*0.4)[:]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "anim = @animate for i ∈ 1:40\n",
    "    plot(sol2(T*0.005*i)[:,1], yaxis = (\"u\", (-1,1)))\n",
    "end\n",
    "\n",
    "gif(anim, \"wave_anim_fps20.gif\", fps = 20)\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#plot([r0[:,1],sol2(T)[:,1],sol(T)[:,1]])\n",
    "#plot(x,sol.u)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.1",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
